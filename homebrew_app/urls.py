from django.conf.urls import include, url#detailed section to get all the requests
from django.contrib import admin

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^', include('main.urls', namespace='main')),
]
