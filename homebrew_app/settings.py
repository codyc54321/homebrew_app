#has all my major settings
"""
Django settings for homebrew_app project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))#main dir for project

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'hhp^-#(lx(h4=e3@zq%on7enee0ilngy=p7jybzm#a&kfuau@i'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (#these apps are imported, like libraries
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # 3rd party
    'django_extensions',
    'rest_framework',
    'corsheaders',

    # custom
    'api',
    'calculations',
    'main',
)

MIDDLEWARE_CLASSES = (#moderator of requests
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'homebrew_app.urls'#taking in requests from these urls

WSGI_APPLICATION = 'homebrew_app.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
DB_HOST = '104.197.156.69'
DB_USER = 'cchilders'
DB_PASSWORD = 'childers9'


# this should be separate, or you'd have to rewrite this entire dictionary when you overwrite settings
# (since the setting overwrite comes at end of the file)
if os.environ.get('HOMEBREW_LOCAL_DB') is not None:
    from .local_db_config import *

print(DB_USER)
print(DB_PASSWORD)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'homebrew',
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST, # an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}


TEMPLATE_DIRS = [os.path.join(BASE_DIR, "templates"),]

TEMPLATE_LOADERS = ('django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader')
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # 'DIRS': [os.path.join(BASE_DIR, "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# STATIC_URL = 'http://storage.googleapis.com/homebrew-test/static/'
STATIC_URL = '/static/'

CORS_ORIGIN_ALLOW_ALL = True

# this will overwrite STATIC_URL, etc for local development
if os.environ.get('LOCAL_DEVELOPMENT') is not None:
    from .settings_dev import *
