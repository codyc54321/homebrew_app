# 3d cart login

https://sandbox-homebrewing-com.3dcartstores.com/admin/login.asp

user:
Cody2016

# Getting Started

Look at the scripts in `scripts/installs/`. Here is an overview:

```
 mkdir projects # or start in your normal location
 cd projects
 git clone git@bitbucket.org:codyc54321/homebrew_app.git
 cd homebrew_app
 # On Windows, install python-pip another way
 sudo apt-get install python-pip
 # On Windows, same here. Install the Windows way
 sudo pip install virtualenv
 virtualenv -p `which python3` env
 # on Windows: source env/Scripts/activate
 source env/bin/activate
 pip3 install -r requirements.txt

 # some tests will fail right now. at least 17 should pass
 python manage.py test

 # this dev server is good enough to see the page
 python manage.py runserver
 # now visit 127.0.0.1:8000/main
```

Now put this in your ~/.bashrc or shell startup script (ex. for zsh, ~/.zshrc) to work locally:

    export LOCAL_DEVELOPMENT=true

If you want to use a local mysql db, also put this in ~/.bashrc or your shell's startup script:

    export HOMEBREW_LOCAL_DB=true


### Daily setup:

```
source {your path}/env/bin/activate
python3 manage.py runserver
```

Now visit `127.0.0.1:8000/main` in browser

## Status:

Google Cloud account:

The 3D Cart API has been split off into a separate process. This will fully update the DB based on the current status of 3D Cart when run.

Look in api/management/commands at update_item_records.

This provides a `python3 manage.py update_item_records` command: https://docs.djangoproject.com/en/1.10/howto/custom-management-commands/

Populating the DB from this 3D Cart API client can be seen in api/management

Run `./manage.py update_item_records`

## Background:

Don't push anything to master, always make branch and open PR

All management/specifics done through Asana

Target is http://www.austinhomebrew.com/

Our app will sit somewhere within existing recipe panel, with goal of replacing much of it
http://www.austinhomebrew.com/Beer-Making-Recipes_c_14.html

Competing site it's based on: https://www.homebrewsupply.com/homebrew-recipe-builder

Worst api ever made:
http://apirest.3dcart.com/Help

If working on setting up DB, read lessons/django_models.html (open in browser)

## Frontend:

Do the 5 tutorials at http://learn.knockoutjs.com/

## Tests:

Read `tests/test_3dcard_api_client.py`

Run: `python manage.py test`

## Deployment

Django API (portion that talks to the frontend) deployed with https://cloud.google.com/python/django/container-engine

Cloud MySQL DB deployed with https://cloud.google.com/sql/docs/quickstart and changing settings.py

Redeploy by:

  ./scripts/redeploy_container.sh

## Other:

#### Only allow integers in input box HTML:
  - http://stackoverflow.com/questions/8808590/html5-number-input-type-that-takes-only-integers
