// hard codes

var HOPS = [
    {
        "category": "Hop Pellets",
        "hops": [
            {
                "name": "Ahtanum Hop Pellets 1 oz",
                "price": 1.99,
                "catalog_id": 1124
            },
            {
                "name": "Amarillo Hop Pellets 1 oz",
                "price": 3.99,
                "catalog_id": 110
            },
            {
                "name": "Apollo (US) Hop Pellets - 1 oz.",
                "price": 2.25,
                "catalog_id": 6577
            },
        ]
    }
]

var FERMENTABLES = [
    {
        "category": "Dry Malt Extract",
        "fermentables": [
            {
                "name": "Briess Bavarian Wheat DME 1 Lb",
                "price": 4.99,
                "catalog_id": 496
            },
            {
                "name": "Briess Bavarian Wheat DME 3 LBS",
                "price": 12.99,
                "catalog_id": 1435
            },
            {
                "name": "Briess Golden Light DME 1 Lb",
                "price": 4.99,
                "catalog_id": 492
            },
        ]
    }
]


var YEASTS = [
    {
        "category": "Dry Beer Yeast",
        "yeasts": [
            {
                "name": "500 g Fermentis Safale S-04",
                "price": 79.99,
                "catalog_id": 6012
            },
            {
                "name": "500 g Fermentis Safale US-05",
                "price": 84.99,
                "catalog_id": 4612
            },
            {
                "name": "500 g Fermentis SafCider Yeast",
                "price": 59.99,
                "catalog_id": 6003
            },
        ]
    }
]

var RECIPE_DATA = [
    {
        id: 31,
        name: "my recipe ",
        notes: "some notes",
        brew_method: "All Grain",
        boil_time: 60,
        batch_size: "4.00",
        fermentable_selections: [
            {
                catalog_id: 496,
                milling_preference: "Unmilled"
            },
            {
                catalog_id: 1435,
                milling_preference: "Milled"
            }
        ],
        hop_selections: [
            {
                catalog_id: 110,
                weight: "4.00",
                minutes: 35,
                use: "Dry Hop"
            }
        ],
        yeast_selections: [
            {
                catalog_id: 6012
            }
        ]
    }
];
