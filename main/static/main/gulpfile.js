'use strict';

var gulp			= require('gulp'),
	sass 			= require('gulp-sass'),
	autoprefixer	= require('gulp-autoprefixer'),
	concat 			= require('gulp-concat'),
	uglify 			= require('gulp-uglify'),
	minify 			= require('gulp-minify');

var sassOptions = {
	errLogToConsole: true,
	outputStyle: 'expanded'
};

// Compile sass into CSS & auto-inject into browsers
gulp.task('main', function() {
	console.log('Compiling Bootstrap 3 assets...');
	gulp.src('stylesheets/**/*.scss')
		.pipe(sass(sassOptions).on('error', sass.logError))
		.pipe(gulp.dest('css/'));
});

gulp.task('compress', function() {
	gulp.src(
		[
			'javascripts/jquery.min.js',
			'javascripts/bootstrap.min.js',
			'javascripts/knockout.min.js',
			'javascripts/underscore.min.js'
		])
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js/'))
});

gulp.task('minify', function() {
	gulp.src(
		[
			'javascripts/app.js'
		])
		.pipe(minify({
			ext:{
				src:'-debug.js',
				min:'.min.js'
			}
		}))
		.pipe(gulp.dest('js/'))
});

// Execute the default task, with the combined tasks
gulp.task('default', ['main', 'compress', 'minify']);
