from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt


@xframe_options_exempt
def main(request):
    return render(request, 'main/index.html')


@xframe_options_exempt
def iframe(request):
    return render(request, 'main/iframe.html')


@xframe_options_exempt
def stripped_down(request):
    return render(request, 'main/stripped-down.html')
