from django.conf.urls import include, url
from django.contrib import admin

from main import views


urlpatterns = [
    url(r'stripped', views.stripped_down, name="stripped-down"),
    url(r'^iframe', views.iframe, name="iframe"),
    url(r'^$', views.main, name="main"),
]
