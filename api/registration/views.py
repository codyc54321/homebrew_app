import json

from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from users.models import AppUser
from utils.general import check_existence


@api_view(['POST'])
def register_user(request):
    """
    Try to register a new user
    """    
    data = json.loads(request.body)
    # user_already_exists = check_existence(AppUser, data) <== this is bad because if the email exists, the user already exists
    try:
        email = data['email']
        KEYS = ['password', 'first_name', 'last_name', 'phone']
        for key in KEYS:
            _ = data[key]
    except:
        return Response(status.HTTP_400_BAD_REQUEST)
    
    users = AppUser.objects.filter(email=email)
    
    user_already_exists = len(AppUser.objects.filter(email=email)) > 0
    if user_already_exists:
        return Response(status.HTTP_303_SEE_OTHER)
        
    new_user = AppUser(**data)
    new_user.save()
    return Response(status=status.HTTP_200_OK)

"""

{"first_name": "Cody", "last_name": "Childers", "email" : "cchilder@mail.usf.edu", "password": 'green party FTW', "phone": "8135452150"}

"""