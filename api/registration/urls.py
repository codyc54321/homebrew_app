from django.conf.urls import url, include
from api.registration import views

urlpatterns = [
    url(r'^register-user/', views.register_user, name='register-user'),
]