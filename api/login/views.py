import json

from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from users.models import AppUser
# from messager.texts import send_text
# from orders.formatting_functions import FUNCTIONS
# from utils.general import check_existence


@api_view(['POST'])
def customer_login(request):
    """
    Try to login a customer (food orderer)
    """
    data = json.loads(request.body)

    try:
        email = data['email']
        password = data['password']
    except:
        return Response(status.HTTP_400_BAD_REQUEST)

    try:
        user = AppUser.objects.get(email=email, password=password)
    except:
        return Response(status.HTTP_401_UNAUTHORIZED)

    return Response(status=status.HTTP_200_OK)

"""

{"email" : "cchilder@mail.usf.edu", "password": "green party FTW"}

"""
