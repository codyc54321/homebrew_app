from django.conf.urls import url, include
from api.login import views

urlpatterns = [
    url(r'^customer-login/', views.customer_login, name='customer-login'),
]