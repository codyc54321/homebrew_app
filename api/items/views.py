import json, time
import yaml

from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from api.models import HopRecord, FermentableRecord, YeastRecord
from api.serializers import ItemRecordSerializer
from utils.general import retrieve_all_objects_and_serialize, unpack_json

ITEM_TYPE_TO_MODEL_MAP = {
    'fermentables': FermentableRecord,
    'hops': HopRecord,
    'yeasts': YeastRecord,
}


def ordered_dict_back_to_regular_dict(ordered_dict):
    regular_dict = {}
    for key in ordered_dict.keys():
        if key == 'price':
            regular_dict[key] = float(ordered_dict[key])
        else:
            regular_dict[key] = ordered_dict[key]
    return regular_dict


def format_item_records(Model, item_type):#this formats the categories
    objects = Model.objects.filter(active=True, in_stock=True)
    categories = Model.objects.order_by('category').values_list('category', flat=True).distinct()
    data = []
    for category in categories:
        category_objects = Model.objects.filter(category=category).order_by('name')
        serializer = ItemRecordSerializer(category_objects, many=True)
        # serializer.data returns OrderedDicts for some reason, blowing up the Knockout frontend
        items = [ordered_dict_back_to_regular_dict(item) for item in serializer.data]
        this_dataset = {'category': category, item_type: items}
        data.append(this_dataset)
    return data


@api_view(['GET'])#taking in a request from api/items, and returns information
def serve_items(request, item_type):
    """
    Serve up some grain
    """
    data = format_item_records(ITEM_TYPE_TO_MODEL_MAP[item_type], item_type)
    return Response(data=data, status=status.HTTP_200_OK)
