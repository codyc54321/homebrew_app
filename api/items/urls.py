
from django.conf.urls import url, include
from api.items import views

urlpatterns = [#allowing a certain tyupe of data to come in, as a string
    url(r'^(?P<item_type>\w+)/', views.serve_items, name='serve-items'),
]
