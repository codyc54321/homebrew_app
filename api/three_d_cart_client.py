#this is how we get 3dCart data
import requests, json, time, os, yaml
from threading import Thread
try:
    from Queue import Queue
except:
    from queue import Queue

from django.conf import settings

from utils.maps import ITEMS_MODELS_MAP#imported from outside class
from api.models import FermentableRecord, HopRecord, YeastRecord
from utils.general import pluck

def write_content(the_file, content):#writes to a file
    with open(the_file, 'w') as f:
        f.write(content)


class ThreeDCartAPIClient(object):#pulling in data, filtering certain things in program, reformatting them, updates our database

    HOST = 'https://apirest.3dcart.com'#path for API
    SECURE_URL = 'https://sandbox-homebrewing-com.3dcartstores.com'
    VERSION = '1'
    PRIVATE_KEY = 'e5612f42602f84f22efd55a8934b0d11'
    TOKEN = 'e89086eaa14a195e75b05b8ace639a00'

    TARGET_URL = HOST + '/3dCartWebAPI/v' + VERSION#this is what we're actually calling, the url

    HEADERS = {#help client and server know what's going on, this is a way for searching for items within a dictionary
        'Content-Type': 'application/json;charset=UTF-8',#what I want to get back
        'Accept': 'application/json',
        'SecureUrl': SECURE_URL,
        'PrivateKey': PRIVATE_KEY,
        'Token': TOKEN,
    }

    # main meaning yeast, hops, and grain
    MAIN_CATEGORIES = [1, 16, 487]#these are the category ids

    # certain categories come back from 1, 16, and 487, but aren't useful. ban them
    """
    ban:

    197 = grains by the bag
    369 = hop pellets by pound
    129 = hop rhizomes
    368 = leaf hops by pound
    384 = yeasts nutrients
    """
    BANNED_CATEGORIES = [129, 197, 368, 369, 384]
    # TODO: NEED TO HAVE A SYSTEM WHERE THE USER CAN BAN SPECIFIC CATEGORIES THEMSELVES
    BANNED_PRODUCTS = []#manually ban products temporarily

    # Wyeast and White Labs each have subcategories we need to grab from
    YEAST_GROUPS = [2, 12]

    def __init__(self):#constructor, fleshing out 3D cart functionality
        #this is calling all of the yaml classes categories
        self.RAW_CATEGORIES_PATH = self.generate_yaml_path('1-raw_categories.yaml')#makes a path to your class
        self.ALL_CATEGORIES_PATH = self.generate_yaml_path('2-all_categories.yaml')
        self.UNPROCESSED_BEER_CATEGORIES_PATH = self.generate_yaml_path('3-unprocessed_beer_categories.yaml')
        self.BEER_CATEGORIES_PATH = self.generate_yaml_path('4-beer_categories.yaml')
        self.FINAL_BEER_CATEGORIES_PATH = self.generate_yaml_path('5-final_beer_categories.yaml')
        self.FINAL_BEER_CATEGORIES_AND_PRODUCTS_PATH = self.generate_yaml_path('6-final_beer_categories_and_products.yaml')

        self.raw_categories = self.get_categories_live_api()#this gets everything from 3D cart, workaround below:
        #replicates return of content from 3dCart
        # self.raw_categories = self.load_from_yaml(self.RAW_CATEGORIES_PATH)#loading from yaml instead of calling API

        #now passing raw categories data to next line
        self.all_categories = self.process_category_keys(self.raw_categories)#takes in data of each all_categories object
        self.dump_to_yaml(self.ALL_CATEGORIES_PATH, self.all_categories)#dumps everything from raw categories into the self.raw_categories object

        self.unprocessed_beer_categories = self.filter_to_beer_ingredient_categories(self.all_categories)#filtered down to 3 categories, 1, 16, 487(i.e. yeast, hops, and grain)
        self.dump_to_yaml(self.UNPROCESSED_BEER_CATEGORIES_PATH, self.unprocessed_beer_categories)#then dump all returned content to the yaml file, using saved path, and placed in class
        #processing...
        self.beer_categories = self.process_beer_ingredients_subcategories(self.unprocessed_beer_categories, self.all_categories)
        self.dump_to_yaml(self.BEER_CATEGORIES_PATH, self.beer_categories)

        self.final_beer_categories = self.format_final_beer_categories(self.beer_categories)
        self.dump_to_yaml(self.FINAL_BEER_CATEGORIES_PATH, self.final_beer_categories)
        # import ipdb; ipdb.set_trace() #this allows us to search through the code in the terminal

        #calls API 20 times, that's why blanked out
        # self.final_beer_categories_including_products = self.add_products_to_final_beer_categories(self.final_beer_categories)
        self.final_beer_categories_including_products = self.load_from_yaml(self.FINAL_BEER_CATEGORIES_AND_PRODUCTS_PATH)
        self.dump_to_yaml(self.FINAL_BEER_CATEGORIES_AND_PRODUCTS_PATH, self.final_beer_categories_including_products)

        #renames fermentables, hops, and yeasts to new categories
        self.fermentables = self.pluck_items_and_rename_them(self.final_beer_categories_including_products, 487, 'fermentables')
        self.hops = self.pluck_items_and_rename_them(self.final_beer_categories_including_products, 16, 'hops')
        self.yeasts = self.pluck_items_and_rename_them(self.final_beer_categories_including_products, 1, 'yeasts')

    # TODO:
    #NEW FUNCTION TO SEE IF PRODUCT IS SOLD OUT
    def check_if_product_is_sold_out(self, catalog_id):
        """ hit API from /Products/{catalog_id} and check if it's sold out """
        pass
    #updates all item records of current object. DOES THIS NEED TO BE COMPLETED?
    def update_all_item_records(self):
        for item_type in ITEMS_MODELS_MAP:#looping through imported dictionary, main list
            dataset = getattr(self, item_type)#get attributes of each item, gets self.hops
            RecordModel = ITEMS_MODELS_MAP[item_type]['record_model']#store each item_type in field, searches by hops, and then my model within hops
            self.update_retrieved_item_records_to_db(dataset, RecordModel, item_type)#put items, with types in database

    #this actually creates database if the database is empty, otherwise, it updates it:
    def update_retrieved_item_records_to_db(self, dataset, Model, items_key):#retrieving three fields, placing in Object
        """
        items_key like 'fermentables', 'hops', 'yeast'
        """
        # if its in saved_catalog_ids but not retrieved_catalog_ids:
        #    it isnt sold any more. set active=False

        # if its in retrieved_catalog_ids but not saved_catalog_ids:
        #    it's new. save it

        # if it existed and still exists:
        #    update it (overwrite it. if it hasn't changed, nothing will happen)

        retrieved_catalog_ids = []
        #looping through dataset
        for category_dataset in dataset:
            """ dataset looks like:

                [
                    {'category': 'Dry Malt Extract',
                    'fermentables': [
                        {'catalog_id': 496, 'name': 'Briess Bavarian Wheat DME 1 Lb', 'price': 4.99},
                        ...
                        ]
                    },
                    {'category': 'Honeys',
                    'fermentables': [
                        {'catalog_id': 555, 'name': 'Wildflower Honey 1 Lb', 'price': 6.99},
                        ...
                        ]
                    },
                    ...
                ]
            """
            category = category_dataset['category']#store category in specific dataset
            items = category_dataset[items_key]#store the items_key in relation to the dataset
            """
            items = [
                {'catalog_id': 555, 'name': 'Wildflower Honey 1 Lb', 'price': 6.99},
                ...
            ]
            """
            these_retrieved_catalog_ids = [item['catalog_id'] for item in items]#store each catalog_id in relation to each item
            retrieved_catalog_ids.extend(these_retrieved_catalog_ids)#adding on new catalog ids
            #sub-loop
            for item_data in items:
                """
                item_data looks like:
                  {'catalog_id': 1793, 'name': 'White Sorghum ( 7lbs.)', 'price': 22.99}
                """
                print(item_data)
                current_catalog_id = item_data['catalog_id']#storing current items catalog id
                potential_item = Model.objects.filter(catalog_id=current_catalog_id)#filter items by current_id
                if not potential_item:#if not an item with a catalog id...
                    item_data.update({'category': category})#update item data with catagory id
                    Model.objects.create(**item_data)#create the item_data
                else:
                    potential_item.update(**item_data)#...else just update the item, don't create
        # set deleted items to inactive
        retrieved_catalog_ids = set(retrieved_catalog_ids)
        saved_catalog_ids = Model.objects.all().values_list('catalog_id', flat=True)
        saved_catalog_ids = set(saved_catalog_ids)
        missing_catalog_ids = saved_catalog_ids - retrieved_catalog_ids#how figure out which ones are missing or not
        for missing_catalog_id in missing_catalog_ids:
            item = Model.objects.get(catalog_id=missing_catalog_id)
            item.active = False
            item.save()

    def pluck_items_and_rename_them(self, final_beer_categories, main_catalog_id, items_key):
        data = self.retrieve_from_data(main_catalog_id, final_beer_categories, key="category_id")#for each dictionairy in final beer categories, grab the category id, retrieves sub-data
        new_data = []#placeholder for retrieved data
        for data_set in data['subcategories']:#if the data uses the key 'subcategories'...
            this_object = {}#this object is empty
            this_object['category'] = data_set['name']#whatever the name is, like 'dog, cat', it will be set to the category title
            this_object[items_key] = data_set['products']#whatever the products is, like buiscut or bread, collect the items key(hops, grain/fermentables, or yeast)
            new_data.append(this_object)#append to the object, which has only collecting product item so far
            import ipdb; ipdb.set_trace()#way to see how code works, and for error checking

        return new_data

    def add_products_to_final_beer_categories(self, final_beer_categories):#getting final_beer_categories
        result_queue = Queue()#create Queue object, hold things to be done, and wait to be done
        worker_threads = []#making thread for each function
        returns_list = []
        for main_category in final_beer_categories:#loop
            main_category_id = main_category['category_id']#store id, hops, yeast, or grain
            for subcategory in main_category['subcategories']:#for each subcategory, make a call to Thread
                worker = Thread(
                    target=self.gather_unique_products_asynchronously,#target function I want
                    args=(subcategory, main_category_id, result_queue)
                )
                worker.start()
                worker_threads.append(worker)#added a worker to the the worker thread and result queue
        for worker in worker_threads:#wait for worker thread to finish for 2 minutes max
            worker.join(120)
        while not result_queue.empty():#if result queue isn't empty, wait
            returns_list.append(result_queue.get())
        # now add the results back in to the subcategories
        for data_set in returns_list:
            for main_category in final_beer_categories:#insert products into correct locations
                if main_category['category_id'] == data_set['main_category_id']:
                    for subcategory in main_category['subcategories']:
                        if subcategory['category_id'] == data_set['subcategory_id']:
                            subcategory['products'] = data_set['results']
        print(final_beer_categories)
        return final_beer_categories

    def gather_unique_products_asynchronously(self, subcategory, main_category_id, queue):
        print("****** starting worker to grab products ******")
        subcategory_id = subcategory['category_id']
        gathered_products = self.gather_products_for_a_subcategory(subcategory)
        unique_products = self.filter_gathered_products_to_only_unique_products(gathered_products)
        sorted_products = self.sort_by_key(unique_products, 'name')
        data = {
            'main_category_id': main_category_id,
            'subcategory_id': subcategory['category_id'],
            'results': sorted_products
        }
        print(data)
        print("****** worker finished ******")
        queue.put(data)

    def filter_gathered_products_to_only_unique_products(self, gathered_products):
        # TODO: why doesnt this just turn a list into a set? investigate
        unique_products = []
        for product in gathered_products:
            product_ids = [p['catalog_id'] for p in unique_products]
            this_product_id = product['catalog_id']
            if not this_product_id in product_ids and not this_product_id in self.BANNED_PRODUCTS:
                unique_products.append(product)
        return unique_products

    def gather_products_for_a_subcategory(self, subcategory):
        gathered_products = []
        # some categories need products from several smaller categories to be joined
        # like if a certain yeast brand has 9 categories, we only want to include the products
        # for that brand as "Wyeast", not as 9 Wyeast categories
        if 'subcategory_ids' in subcategory.keys():
            for this_id in subcategory['subcategory_ids']:
                products = self.get_products(this_id)
                processed_products = [self.process_product(product) for product in products]
                gathered_products.extend(processed_products)
        else:
            products = self.get_products(subcategory['category_id'])
            processed_products = [self.process_product(product) for product in products]
            gathered_products.extend(processed_products)
        return gathered_products



    def format_final_beer_categories(self, beer_categories):
        main_categories = self.get_main_categories(beer_categories)#getting main categories of beer categories
        for category in main_categories:#1, 16, 487 (hops, grain, yeast)
            subcategories = self.get_subcategories(category, beer_categories)#find subcategories from beer categroeis, matches 1 to parent of 1...
            category['subcategories'] = subcategories #puts in dictionary, with subcategories key
        return main_categories

    def process_product(self, product):
        # find attributes like alpha_acids, etc in feature list
        feature_list = sku_info = product['FeatureList']

        sku_info = product['SKUInfo']
        MAP = {
            'name': 'Name',
            'catalog_id': 'CatalogID',
            'price': 'Price',

        }
        processed_product = {}
        for k, v in MAP.items():
            processed_product[k] = sku_info.get(v, '')
        return processed_product

    def get_main_categories(self, categories):
        return [item for item in categories if item['category_id'] in self.MAIN_CATEGORIES]

    def get_subcategories(self, category, categories):
        return [item for item in categories if item['parent'] == category['category_id'] ]

    def process_beer_ingredients_subcategories(self, beer_categories, all_categories):#import beer_categories and all_categories
        for c in beer_categories:#for each category in beer_categories...    #list comprehension example:
            this_id = c['category_id']#store this category_id
            if this_id in self.YEAST_GROUPS:#sub-loop, if this id is one of the things that have sub-categroies..stored variable at top (y-yeast (2) and whitelabs(12))
                subcategory_ids = [cat['category_id'] for cat in all_categories if cat['parent'] == this_id]#go through categoires, if categories parent is whitelabs, it must be subcategory of whitelabs
                c.update({'subcategory_ids': subcategory_ids})#then update subcategories, adds dictionary into other one
        return beer_categories

    def filter_to_beer_ingredient_categories(self, all_categories):#import all_categories into the object
        beer_categories = []#create an array to store all of the beer_categories
        for c in all_categories:#loop through each item in all_categories...
            if c['category_id'] in self.MAIN_CATEGORIES or c['parent'] in self.MAIN_CATEGORIES \
                    and c['category_id'] not in self.BANNED_CATEGORIES:#if the category_id or it's a parent and the category_id isn't banned...
                beer_categories.append(c)#append the category the the beer-categories object
        return beer_categories#...return beer_categories object
    #processing category keys, renaming files taken from yaml file (1-raw_categories)
    def process_category_keys(self, categories):
        categories = self.process_object_data(categories,
            [
                ('name', 'CategoryName'),
                ('title', 'Title'),
                ('category_id', 'CategoryID'),
                ('parent', 'CategoryParent'),
            ]
        )
        return categories

    def process_object_data(self, the_list, list_of_key_value_pairs, sorting_key="title"):
        data = []
        for item in the_list:
            obj = {}
            for key, value in list_of_key_value_pairs:
                obj[key] = item[value]
            data.append(obj)
        final_data = self.sort_by_key(data, sorting_key)
        return final_data

    def get_categories_live_api(self):
        categories = self.get_response( ('Categories',))
        return categories

    def get_products(self, category_id):
        return self.get_response( ('Categories', str(category_id), 'Products'))

    def get_response(self, url_args, limit=1000): #method that makes an url, and limits it to items to 1000
        url = self.get_target_url(url_args, limit)
        return self.return_data(url, self.HEADERS)

    def get_target_url(self, args, limit=1000):#this creates the structure of the url itself
        tail = ""
        for arg in args:
            tail += "/" + str(arg)
        return self.TARGET_URL + tail + "?limit=%s" % str(limit)

    def return_data(self, url, headers=None):
        if headers:
            response = requests.get(url, headers=headers)
        else:
            response = requests.get(url, headers=self.HEADERS)
        return response.json()

    def retrieve_from_data(self, value, the_list, key="category_id"):#using the value, the lsit, and the key, retrive data
        for item in the_list:#loop...
            if item[key] == value:#if each item's key is the same as the value, return the item (completely generic)
                return item
        return None#else, retun nothing

    def sort_by_key(self, data, key):
        return sorted(data, key=lambda this_data: this_data[key])

    def dump_to_yaml(self, filepath, data):
        with open(filepath, 'w') as yaml_file:
            yaml.dump(data, yaml_file, default_flow_style=False)

    def load_from_yaml(self, filepath):
        with open(filepath, 'r') as stream:
            data = yaml.load(stream)
            return data

    def generate_yaml_path(self, filename):
        return os.path.join(settings.BASE_DIR, "yamls", filename)
