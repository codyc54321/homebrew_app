from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# BREW_METHOD = (
#     ('ex', 'Extract'),
#     ('mm', 'Mini-Mash'),
#     ('ag', 'All Grain'),
#     ('bb', 'Brew-in-a-bag'),
# )

# MILLING_PREFERENCES = (
#     ('mi', 'Milled'),
#     ('um', 'Unmilled'),
# )

# HOPS_USES = (
#     ('dry', 'Dry Hop'),
#     ('boil', 'Boil'),
# )


class Recipe(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=250)
    brew_method = models.CharField(max_length=40) # choices=BREW_METHOD
    batch_size = models.DecimalField(max_digits=5, decimal_places=2)
    # style field in limbo
    notes = models.TextField(null=True, blank=True)
    # TODO: dont forget to enforce db constraints on the frontend
    boil_time = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # TODO: add indexes for filtering like by date ordered, etc


class FermentableSelection(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='fermentables_selections')
    catalog_id = models.IntegerField()
    milling_preference = models.CharField(max_length=40, null=True, blank=True)
    # weight = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return "{} - catalog_id: {}".format(self.recipe.name, self.catalog_id)


class HopSelection(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='hops_selections')
    catalog_id = models.IntegerField()
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    minutes = models.IntegerField()
    use = models.CharField(max_length=40) # choices=HOPS_USES

    def __str__(self):
        return "{} - catalog_id: {}".format(self.recipe.name, self.catalog_id)


class YeastSelection(models.Model):
    recipe = models.ForeignKey(Recipe, related_name='yeasts_selections')
    catalog_id = models.IntegerField()

    def __str__(self):
        return "{} - catalog_id: {}".format(self.recipe.name, self.catalog_id)


#structure for the table
class ItemRecord(models.Model):
    # TODO: check if incoming catalog_ids match a catalog_id/name combo
    # TODO: ask J what would happen if catalog_id changed in 3D Cart so we can use primary key as catalog_id
    catalog_id = models.IntegerField()
    name = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    active = models.BooleanField(default=True) # is the item being sold at all? (carried)
    in_stock = models.BooleanField(default=True) # is the item currently in stock?
    category = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        abstract = True


#inherits ItemRecord properties
class FermentableRecord(ItemRecord):#record of each item
    pass
    # lovibond_value = models.IntegerField()


class HopRecord(ItemRecord):
    pass
    # alpha_acids = = models.DecimalField(max_digits=5, decimal_places=2)

class YeastRecord(ItemRecord):
    pass


class ItemOrder(models.Model):
    catalog_id = models.IntegerField()
    quantity = models.DecimalField(max_digits=7, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
