from rest_framework import serializers
from api.models import (
    Recipe,
    HopRecord, FermentableRecord, YeastRecord,
    FermentableSelection, HopSelection, YeastSelection
)


class FermentableSelectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FermentableSelection
        fields = ('catalog_id', 'milling_preference')


class HopSelectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = HopSelection
        fields = ('catalog_id', 'weight', 'minutes', 'use')


class YeastSelectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = YeastSelection
        fields = ('catalog_id',)


class ItemRecordSerializer(serializers.Serializer):
    catalog_id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=250)
    price = serializers.DecimalField(max_digits=7, decimal_places=2)


class RecipeSerializer(serializers.ModelSerializer):
    fermentables_selections = FermentableSelectionSerializer(many=True, read_only=True)
    hops_selections = HopSelectionSerializer(many=True, read_only=True)
    yeasts_selections = YeastSelectionSerializer(many=True, read_only=True)

    class Meta:
        model = Recipe
        fields = (
            'id', 'name', 'notes', 'brew_method', 'boil_time', 'batch_size',
            'fermentables_selections', 'hops_selections', 'yeasts_selections'
        )
