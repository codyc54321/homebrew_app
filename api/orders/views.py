#this responds to the request, and gives a response

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from api.models import ItemOrder


@api_view(['POST'])
def order_made(request, catalog_id, quantity):
    item_order = ItemOrder(catalog_id=catalog_id, quantity=quantity)
    item_order.save()
    return Response(status=status.HTTP_200_OK)
