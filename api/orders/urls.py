from django.conf.urls import url, include # Tells Django what requests to allow in
from api.orders import views

urlpatterns = [
    url(r'^(?P<catalog_id>\d+)[/](?P<quantity>\d+)$', views.order_made, name='order-made'),
]
