
from utils.general import parse_keys, pluck
from api.models import (
    Recipe, HopSelection,
    FermentableSelection, YeastSelection
)

RECIPE_OBJECT_KEYS = ['name', 'brew_method', 'batch_size', 'boil_time', 'notes']

SELECTION_MODELS_MAP = {'hops': HopSelection, 'fermentables': FermentableSelection, 'yeasts': YeastSelection}

"""
{
    u'boil_time': u'60',
    u'hops': [
        {
            u'amount': u'4', u'use': u'Dry Hop',
            u'catalog_id': 114, u'time': u'15'
        }
    ],
    u'yeasts': [{u'catalog_id': 1045}],
    u'notes': u'some notes', u'beer_style': u'Standard American Beer',
    u'batch_size': u'5',
    u'fermentables': [{u'catalog_id': 1432}],
    u'recipe_name': u'my recipe', u'brew_method': u'Extract'
}
"""


def delete_recipe_selection(Model, **kwargs):
    obj_list = Model.objects.filter(**kwargs)
    if obj_list:
        obj_list[0]


def create_specific_selections(data_sets, recipe, Model):
    if data_sets:
        for data_set in data_sets:
            item = Model(
                recipe=recipe,
                **data_set
            )
            item.save()


def create_recipe_selections(data, recipe):
    for key in SELECTION_MODELS_MAP.keys():
        create_specific_selections(data_sets=data[key], recipe=recipe, Model=SELECTION_MODELS_MAP[key])


def create_recipe(data, user):
    recipe = Recipe(
        user=user, # TODO: hardcoded for testing; give it real user id from page
        **parse_keys(data, RECIPE_OBJECT_KEYS)
    )
    recipe.save()
    create_recipe_selections(data, recipe)

# TODO: rewrite this: http://www.django-rest-framework.org/api-guide/relations/#writable-nested-serializers
# http://stackoverflow.com/questions/33077256/django-rest-framework-updating-a-foreign-key
def update_recipe_selections(data, recipe_id):
    recipe = Recipe.objects.get(id=recipe_id)
    for key in SELECTION_MODELS_MAP.keys():
        Model = SELECTION_MODELS_MAP[key]
        # print(data)
        previous_catalog_ids = set(Model.objects.filter(recipe=recipe).values_list('catalog_id', flat=True))
        print(Model)
        print(previous_catalog_ids)
        received_selections_data = data[key]
        received_catalog_ids = set(pluck(received_selections_data, 'catalog_id'))
        new_catalog_ids = received_catalog_ids - previous_catalog_ids
        print("new:")
        print(new_catalog_ids)

        for data_set in received_selections_data:
            if data_set['catalog_id'] in new_catalog_ids:
                new_selection = Model(recipe=recipe, **data_set)
                new_selection.save()

        old_catalog_ids = previous_catalog_ids - received_catalog_ids
        print("old:")
        print(old_catalog_ids)

        for this_id in old_catalog_ids:
            item = Model.objects.get(recipe=recipe, catalog_id=this_id)
            item.delete()


def update_recipe(data):
    recipe_id = data['recipe_id']
    Recipe.objects.filter(id=recipe_id).update(**parse_keys(data, RECIPE_OBJECT_KEYS))
    update_recipe_selections(data, recipe_id)
