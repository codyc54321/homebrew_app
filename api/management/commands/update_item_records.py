from django.core.management.base import BaseCommand

from api.three_d_cart_client import ThreeDCartAPIClient


class Command(BaseCommand):
    help = 'Updates all item records after calling 3D Cart API'

    def handle(self, *args, **options):
        client = ThreeDCartAPIClient()
        client.update_all_item_records()
