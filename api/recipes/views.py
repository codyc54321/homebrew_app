#this responds to the request, and gives a response

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from api.serializers import RecipeSerializer
from api.models import Recipe
from utils.recipes import create_recipe, update_recipe
from utils.general import get_example_user


@api_view(['DELETE', 'PUT'])
def delete_or_update_recipe(request, recipe_id):

    if request.method == 'DELETE':
        recipe = get_object_or_404(Recipe, pk=recipe_id)
        recipe.delete()
        return Response(status=status.HTTP_200_OK)

    if request.method == 'PUT':
        data = request.data
        update_recipe(data)
        return Response(status=status.HTTP_200_OK)


@api_view(['GET', 'POST',]) # giving us data to do something with
def list_or_create_recipes(request):

    if request.method == 'GET':
        recipes = Recipe.objects.all()
        recipe_serializer = RecipeSerializer(recipes, many=True)
        return Response(data=recipe_serializer.data, status=status.HTTP_200_OK)

    elif request.method == 'POST':
        data = request.data
        """
        {
            u'boil_time': u'60',
            u'hops': [
                {
                    u'amount': u'4', u'use': u'Dry Hop',
                    u'catalog_id': 114, u'time': u'15'
                }
            ],
            u'yeasts': [{u'catalog_id': 1045}],
            u'notes': u'some notes', u'beer_style': u'Standard American Beer',
            u'batch_size': u'5',
            u'fermentables': [{u'catalog_id': 1432}],
            u'recipe_name': u'my recipe', u'brew_method': u'Extract'
        }
        """
        user = get_example_user()
        create_recipe(data, user)
        return Response(status=status.HTTP_200_OK)
