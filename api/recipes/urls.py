from django.conf.urls import url, include # Tells Django what requests to allow in
from api.recipes import views

urlpatterns = [
    url(r'^(?P<recipe_id>\d+)[/]?$', views.delete_or_update_recipe, name='delete-or-update-recipe'),
    url(r'^$', views.list_or_create_recipes, name='list-or-create-recipes'),
]
