from django.conf.urls import url, include

urlpatterns = [
    url(r'^items/', include('api.items.urls', namespace='items')),
    url(r'^recipes[/]?', include('api.recipes.urls', namespace='recipes')),
    url(r'^orders/', include('api.orders.urls', namespace='orders')),
]
