"""
categories keys

['AdditionalKeywords', 'AllowAccess', 'CategoryDescription', 'CategoryFooter', 'CategoryHeader', 'CategoryID', 'CategoryIcon',
'CategoryMain', 'CategoryMenuGroup', 'CategoryName', 'CategoryParent', 'CustomFileName', 'DefaultProductsSorting',
'DisplayTypeCategoryGeneralProducts', 'DisplayTypeCategorySpecialItems', 'DisplayTypeRelatedItems', 'DisplayTypeUpsellItems',
'FilterCategory', 'Hide', 'HideLeftBar', 'HideRightBar', 'HideTopMenu', 'HomeSpecialCategory', 'ItemsPerPageCategoryGeneralItems',
'ItemsPerPageCategorySpecialItems', 'LastUpdate', 'Link', 'MetaTags',
'OnFailRedirectTo', 'OptionSetList', 'ProductColumnsCategoryGeneralItems',
'ProductColumnsCategorySpecials', 'ProductColumnsRelatedProducts',
'ProductColumnsUpsellProducts', 'SmartCategories', 'SmartCategoriesLinkTarget',
'SmartCategoriesSearchKeyword', 'Sorting', 'SubcategoryColumnsCategorySpecials', 'TemplateCategoryPage', 'TemplateProductPage',
'Title', 'UserID']
"""
