// hard codes

var HOPS = [
    {
        "category": "Hop Pellets",
        "hops": [
            {
                "name": "Ahtanum Hop Pellets 1 oz",
                "price": 1.99,
                "catalog_id": 1124
            },
            {
                "name": "Amarillo Hop Pellets 1 oz",
                "price": 3.99,
                "catalog_id": 110
            },
            {
                "name": "Apollo (US) Hop Pellets - 1 oz.",
                "price": 2.25,
                "catalog_id": 6577
            },
        ]
    }
]

var FERMENTABLES = [
    {
        "category": "Dry Malt Extract",
        "fermentables": [
            {
                "name": "Briess Bavarian Wheat DME 1 Lb",
                "price": 4.99,
                "catalog_id": 496
            },
            {
                "name": "Briess Bavarian Wheat DME 3 LBS",
                "price": 12.99,
                "catalog_id": 1435
            },
            {
                "name": "Briess Golden Light DME 1 Lb",
                "price": 4.99,
                "catalog_id": 492
            },
        ]
    }
]


var YEASTS = [
    {
        "category": "Dry Beer Yeast",
        "yeasts": [
            {
                "name": "500 g Fermentis Safale S-04",
                "price": 79.99,
                "catalog_id": 6012
            },
            {
                "name": "500 g Fermentis Safale US-05",
                "price": 84.99,
                "catalog_id": 4612
            },
            {
                "name": "500 g Fermentis SafCider Yeast",
                "price": 59.99,
                "catalog_id": 6003
            },
        ]
    }
]

var RECIPE_DATA = [
    {
        id: 31,
        name: "my recipe ",
        notes: "some notes",
        brew_method: "All Grain",
        boil_time: 60,
        batch_size: "4.00",
        fermentable_selections: [
            {
                catalog_id: 496,
                milling_preference: "Unmilled"
            },
            {
                catalog_id: 1435,
                milling_preference: "Milled"
            }
        ],
        hop_selections: [
            {
                catalog_id: 110,
                weight: "4.00",
                minutes: 35,
                use: "Dry Hop"
            }
        ],
        yeast_selections: [
            {
                catalog_id: 6012
            }
        ]
    }
];

var API_BASE = "http://104.154.74.141";
// var API_BASE = "127.0.0.1:8000";

ko.observableArray.fn.countVisible = function(){
    return ko.computed(function(){
        var items = this();

        if (items === undefined || items.length === undefined){
            return 0;
        }

        var visibleCount = 0;

        for (var index = 0; index < items.length; index++){
            if (items[index]._destroy != true){
                visibleCount++;
            }
        }

        return visibleCount;
    }, this)();
};

function Fermentable(data) {
    var self = this;
    var options = data.options;
    self.fermentables_options = ko.computed(function(){
        return options;
    });
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "");
    self.milling_preference = ko.observable(data.milling_preference || "Milled");

    self.is_valid = ko.computed(function(){
        var valid = self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function Hop(data) {
    var self = this;
    self.hops_options = ko.computed(function(){
        return data.options;
    });
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "");
    self.weight = ko.observable(data.weight || "");
    self.minutes = ko.observable(data.minutes || "");
    self.use = ko.observable(data.use || "Boil");

    self.is_valid = ko.computed(function(){
        var valid = self.weight() > 0 && self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function Yeast(data){
    var self = this;
    var permanent_yeasts_options = data.options;
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "-");
    self.current_filter = ko.observable("-Any-");
    self.yeast_groups_individual = ko.computed(function(){
            if (self.current_filter() !== "-Any-"){
                var options = _.filter(data.options, function(option){
                    return option.category === self.current_filter();
                });
                return options;
            } else{
                return permanent_yeasts_options;
            }
        }
    );
    self.yeast_categories = ko.observableArray();
    ko.computed(function(){
        var starter_list = ['-Any-'];
        var categories = _.pluck(permanent_yeasts_options, 'category');
        var final = starter_list.concat(categories);
        self.yeast_categories(final);
    });

    self.is_valid = ko.computed(function(){
        var valid = self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function RecipeViewModel() {

    var self = this;

    self.get_data = function(url_ending){
        // var URL = API_BASE + "/api/&/".replace("&", url_ending);
        var URL = "/api/&/".replace("&", url_ending);
        return $.ajax({
            dataType: "json",
            url: URL,
        });
    }

    self.styles = [
        "--",
        "Standard American Beer",
        "International Lager",
        "Czech Lager",
        "Pale Malty European Lager",
        "Pale Bitter European Beer",
        "Amber Malty European Lager",
        "Amber Bitter European Lager",
        "Dark European Lager"
    ]
    self.styles_data = ko.observableArray();
    ko.computed(function(){
        // adds blank entry to styles_data, and converts the list to a value/text pair for each item
        var data = [];
        for (i = 0; i < self.styles.length; i++){
            var text = self.styles[i];
            if (text === "--"){
                data.push({value: "--", display: "--"});
            } else {
                var display_text = i.toString() + ". " + text;
                var this_entry = {value: text, display: display_text};
                data.push(this_entry);
            }
        }
        self.styles_data(data);
    });

    // used to determine if spinning loading symbol should be showed or not
    self.recipes_loaded = ko.observable(false);
    self.recipes = ko.observableArray();

    self.current_style = ko.observable("--");

    // defaults
    self.total_price = ko.observable(0.0); // TODO: this should not default if the recipe has items already...
    self.hops_uses = ko.observableArray(['Boil', 'Dry Hop']);
    self.weight_units = ko.observableArray(['oz', 'lb']);
    self.milling_preferences = ko.observableArray(['Milled', 'Unmilled']);
    self.brew_methods = ko.observableArray(['Extract', 'Mini-Mash', 'All Grain', 'Brew-in-a-bag']);

    self.set_defaults = function(){
        // start of input fields
        self.id = ko.observable(null);
        self.name = ko.observable("");
        self.brew_method = ko.observable("Extract");
        self.batch_size = ko.observable("5");
        self.beer_style = ko.observable("Standard American Beer");
        self.boil_time = ko.observable("60");
        self.notes = ko.observable("");
        self.fermentables = ko.observableArray(
            [
                new Fermentable({options: {} }),
                new Fermentable({options: {} })
            ]
        );
        self.hops = ko.observableArray([new Hop({options: {} }), new Hop({options: {} })]);
        self.yeasts = ko.observableArray([new Yeast({options: {} })]);
    }

    self.set_defaults();

    self.recipes_loaded_success_callback = function(data) {
        self.recipes_loaded(true);
        self.recipes(data);
    }

    self.items_loaded = function(data,  textStatus, jqXHR, item_type, Model, how_many) {
        console.log(data);
        self[item_type + '_options'] = data;
        var new_items = [];
        for (i = 0; i < how_many; i++) {
            new_items.push(new Model( {options: self[item_type + '_options']} ));
        }
        self[item_type](new_items);
    }

    self.initial_load = function() {
        self.get_data("recipes").done(self.recipes_loaded_success_callback);
        self.get_data("items/fermentables").done(function(a,b,c) { self.items_loaded(a,b,c, 'fermentables', Fermentable, 2) });
        self.get_data("items/hops").done(function(a,b,c) { self.items_loaded(a,b,c, 'hops', Hop, 2) });
        self.get_data("items/yeasts").done(function(a,b,c) { self.items_loaded(a,b,c, 'yeasts', Yeast, 1) });
    }

    self.initial_load();

    // somehow, these 2 functions have to be instantiated only after all 3 item callbacks are finished
    // maybe chain them together and do a final .done?

    // self.prices_hash = ko.computed(function(){
    //     var data = {};
    //     var strings = ['fermentables', 'hops', 'yeasts'];
    //     for (i = 0; i < strings.length; i++) {
    //         var string = strings[i];
    //         var attr = strings[i] + '_options';
    //         for (j = 0; j < self[attr].length; j++) {
    //             var groups = self[attr][j][string];
    //             for (k = 0; k < groups.length; k++) {
    //                 var catalog_id = groups[k].catalog_id.toString();
    //                 var current_price = groups[k].price;
    //                 data[catalog_id] = current_price;
    //             }
    //         }
    //     }
    //     return data;
    // });
    //
    // self.current_price = ko.computed(function(){
    //     var total_price = 0;
    //     var strings = ['fermentables', 'hops', 'yeasts'];
    //     for (i = 0; i < strings.length; i++) {
    //         /* for each item in valid_fermentables, valid_hops, and valid_yeasts,
    //          add the price (if there is one) to total_price */
    //         var attr = strings[i];
    //         var these_valid_items = self.valid_items( self[attr]() ); // self.yeasts, self.hops, etc
    //         for (j = 0; j < these_valid_items.length; j++){
    //             var item = these_valid_items[j];
    //             total_price = total_price + self.prices_hash()[item.catalog_id()];
    //         }
    //     }
    //
    //     return total_price.toFixed(2);
    // });

    self.reset_form = function(){
        self.id(null)
        self.name("");
        self.brew_method("Extract");
        self.batch_size("5");
        self.beer_style("Standard American Beer");
        self.boil_time("60");
        self.notes("");
        self.fermentables(
            [
                new Fermentable({options: self.fermentables_options}),
                new Fermentable({options: self.fermentables_options})
            ]
        );
        self.hops([new Hop({options: self.hops_options}), new Hop({options: self.hops_options})]);
        self.yeasts([new Yeast({options: self.yeasts_options})]);
    }

    self.populate_recipe = function(data, event){
        var context = ko.contextFor(event.target);
        var index = context.$index();
        var recipe = self.recipes()[index];
        var attrs = ['id', 'name', 'brew_method', 'boil_time', 'batch_size', 'notes']
        for (i = 0; i < attrs.length; i++) {
            attr = attrs[i];
            self[attr](recipe[attr]);
        }

        var populate_items = function(item_type, Model) {
            var data = recipe[item_type + '_selections'] // like fermentables_selections
            var new_data = [];
            for (k = 0; k < data.length; k++) {
                // makes a new Fermentable, Hop, or Yeast object
                var data_set = data[k];
                data_set['options'] = self[item_type + '_options'] // like fermentables_options
                var this_item = new Model(data_set);
                new_data.push(this_item);
            }
            self[item_type](new_data);
        }

        var populate_items_data = [
            {item_type: 'fermentables', model: Fermentable},
            {item_type: 'hops', model: Hop},
            {item_type: 'yeasts', model: Yeast},
        ]

        for (n = 0; n < populate_items_data.length; n++) {
            var this_data = populate_items_data[n];
            populate_items(this_data.item_type, this_data.model);
        }
    }

    self.valid_items = function(items){
        var undestroyed_items = _.filter(items, function(item){
            return !("_destroy" in item);
        });

        var final_items = _.filter(undestroyed_items, function(item){
            return item.is_valid();
        });
        return final_items;
    }

    self.addItem = function(item_name) {
        var item_data = {fermentables: Fermentable, hops: Hop, yeasts: Yeast};
        var Model = item_data[item_name];
        self[item_name].push(new Model({options: self[item_name + '_options']}));
    }

    // http://stackoverflow.com/questions/40501838/pass-string-parameters-into-click-binding-while-retaining-default-params-knockou
    self.removeItem = function(item, item_name){
        self[item_name].destroy(item);
    }

    self.purify_items = function(items, attrs){
        // TODO: check if can be replaced by pluck or something similar in _.js
        /* retrieves only the relevant attributes from a list of objects
         given a list like
           [ {catalog_id: 404, weight: 3, minutes: 45, use: 'Dry hop'}, ]
         and attrs ['catalog_id', 'weight'] it will return only
           [ {catalog_id: 404, weight: 3}, ]
         */
        var final_items = [];
        for (i = 0; i < items.length; i++){
            var item = items[i];
            var object = {};
            for (j = 0; j < attrs.length; j++) {
                var attr = attrs[j];
                object[attr] = item[attr];
            }
            final_items.push(object);
        }
        return final_items;
    }

    self.prepareJSON = function(){
        // pure as in only the fields the server cares about
        var pure_fermentables = self.purify_items(self.valid_items( self.fermentables() ), ['catalog_id', 'milling_preference']);
        var pure_hops = self.purify_items(self.valid_items( self.hops() ), ['catalog_id', 'weight', 'minutes', 'use']);
        var pure_yeasts = self.purify_items(self.valid_items( self.yeasts() ), ['catalog_id']);

        object = {
            recipe_id: self.id(),
            fermentables: pure_fermentables,
            hops: pure_hops,
            yeasts: pure_yeasts,
            name: self.name(),
            brew_method: self.brew_method(),
            batch_size: self.batch_size(),
            beer_style: self.beer_style(),
            boil_time: self.boil_time(),
            notes: self.notes(),
        }
        return object;
    }

    self.updateRecipes = function() {
        self.recipes_loaded(false);
        self.get_data("recipes").done(self.recipes_loaded_success_callback);
    }

    self.saveRecipeData = function(){
        var recipe_data = ko.toJSON(self.prepareJSON());
        // http://stackoverflow.com/questions/28921127/how-to-wait-for-a-javascript-promise-to-resolve-before-resuming-function
        // https://developers.google.com/web/fundamentals/getting-started/primers/promises
        var call_save = function(the_recipe_data) {
            if (self.id() == '' || self.id() == null) {
                return $.ajax({
                    // url: API_BASE +  "/api/recipes/",
                    url: "/api/recipes/",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "POST",
                    dataType: "json",
                    data: the_recipe_data,
                    complete: function(data) {
                        self.updateRecipes();
                    }
                });
            } else {
                var recipe_id = self.id();
                return $.ajax({
                    url: "/api/recipes/%".replace('%', recipe_id ),
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "PUT",
                    dataType: "json",
                    data: the_recipe_data,
                    complete: function(data) {
                        self.updateRecipes();
                    }
                    // error: function(e) {
                    //     console.log(e);
                    // }
                });
            }
        }

        call_save(recipe_data);
    }

    self.delete_recipe = function(data, event){
        console.log("running delete_recipe");
        var recipe_id = data.id;
        // var URL = API_BASE + "/api/recipes/&/".replace("&", recipe_id);
        var URL = "/api/recipes/&/".replace("&", recipe_id);
        $.ajax({
            url: URL,
            headers: {
                "Content-Type": "application/json"
            },
            // async: false,
            method: "DELETE",
            dataType: "json",
            complete: function(data) {
                self.updateRecipes();
            }
        });

        // self.get_data("recipes").done(self.recipes_loaded_success_callback);
    }
}
ko.applyBindings(new RecipeViewModel());
