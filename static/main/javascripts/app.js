

var API_BASE = "http://104.154.74.141";
// var API_BASE = "127.0.0.1:8000";

ko.observableArray.fn.countVisible = function(){
    return ko.computed(function(){
        var items = this();

        if (items === undefined || items.length === undefined){
            return 0;
        }

        var visibleCount = 0;

        for (var index = 0; index < items.length; index++){
            if (items[index]._destroy != true){
                visibleCount++;
            }
        }

        return visibleCount;
    }, this)();
};

ko.observableArray.fn.getVisible = function(){
    return ko.computed(function(){
        var items = this();
        var valid_items = [];
        for (m = 0; m < items.length; m++){
            var this_item = items[m]
            if (this_item._destroy != true && this_item != undefined && this_item != null){
                valid_items.push(this_item);
            }
        }
        return valid_items;

    }, this)();
};

function Fermentable(data) {
    var self = this;
    var options = data.options;
    self.fermentables_options = ko.computed(function(){
        return options;
    });
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "");
    self.quantity = ko.observable(data.quantity || "1");
    self.milling_preference = ko.observable(data.milling_preference || "Milled");

    self.is_valid = ko.computed(function(){
        var valid = self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function Hop(data) {
    var self = this;
    self.hops_options = ko.computed(function(){
        return data.options;
    });
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "");
    self.quantity = ko.observable(data.quantity || "1");
    // self.weight = ko.observable(data.weight || "");
    // self.minutes = ko.observable(data.minutes || "");
    // self.use = ko.observable(data.use || "Boil");

    self.is_valid = ko.computed(function(){
        var valid = self.quantity() > 0 && self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function Yeast(data){
    var self = this;
    var permanent_yeasts_options = data.options;
    self.catalog_id = ko.observable(data.catalog_id || "");
    self.name = ko.observable(data.name || "-");
    self.quantity = ko.observable(data.quantity || "1");
    self.current_filter = ko.observable("-Any-");
    self.yeast_groups_individual = ko.computed(function(){
            if (self.current_filter() !== "-Any-"){
                var options = _.filter(data.options, function(option){
                    return option.category === self.current_filter();
                });
                return options;
            } else{
                return permanent_yeasts_options;
            }
        }
    );
    self.yeast_categories = ko.observableArray();
    ko.computed(function(){
        var starter_list = ['-Any-'];
        var categories = _.pluck(permanent_yeasts_options, 'category');
        var final = starter_list.concat(categories);
        self.yeast_categories(final);
    });

    self.is_valid = ko.computed(function(){
        var valid = self.catalog_id() !== "" && self.catalog_id() !== "-";
        return valid
    });
}

function RecipeViewModel() {

    var self = this;

    self.get_data = function(url_ending){
        // var URL = API_BASE + "/api/&/".replace("&", url_ending);
        var URL = "/api/&/".replace("&", url_ending);
        return $.ajax({
            dataType: "json",
            url: URL,
        });
    }

    self.styles = [
        "--",
        "Standard American Beer",
        "International Lager",
        "Czech Lager",
        "Pale Malty European Lager",
        "Pale Bitter European Beer",
        "Amber Malty European Lager",
        "Amber Bitter European Lager",
        "Dark European Lager"
    ]
    self.styles_data = ko.observableArray();
    ko.computed(function(){
        // adds blank entry to styles_data, and converts the list to a value/text pair for each item
        var data = [];
        for (i = 0; i < self.styles.length; i++){
            var text = self.styles[i];
            if (text === "--"){
                data.push({value: "--", display: "--"});
            } else {
                var display_text = i.toString() + ". " + text;
                var this_entry = {value: text, display: display_text};
                data.push(this_entry);
            }
        }


        self.styles_data(data);
    });

    // used to determine if spinning loading symbol should be showed or not
    self.recipes_loaded = ko.observable(false);
    self.recipes = ko.observableArray();

    self.current_style = ko.observable("--");

    // defaults
    self.total_price = ko.observable(0.0); // TODO: this should not default if the recipe has items already...
    self.hops_uses = ko.observableArray(['Boil', 'Dry Hop']);
    self.weight_units = ko.observableArray(['oz', 'lb']);
    self.milling_preferences = ko.observableArray(['Milled', 'Unmilled']);
    self.brew_methods = ko.observableArray(['Extract', 'Mini-Mash', 'All Grain', 'Brew-in-a-bag']);

    self.set_defaults = function(){
        // start of input fields
        self.id = ko.observable(null);
        self.name = ko.observable("");
        self.brew_method = ko.observable("Extract");
        self.batch_size = ko.observable("5");
        self.beer_style = ko.observable("Standard American Beer");
        self.boil_time = ko.observable("60");
        self.notes = ko.observable("");
        self.fermentables = ko.observableArray(
            [
                new Fermentable({options: {} }),
                new Fermentable({options: {} })
            ]
        );
        self.hops = ko.observableArray([new Hop({options: {} }), new Hop({options: {} })]);
        self.yeasts = ko.observableArray([new Yeast({options: {} })]);
    }

    self.set_defaults();

    self.recipes_loaded_success_callback = function(data) {
        self.recipes_loaded(true);
        self.recipes(data);
    }

    self.items_loaded = function(data,  textStatus, jqXHR, item_type, Model, how_many) {
        console.log(data);
        self[item_type + '_options'] = data;
        var new_items = [];
        for (i = 0; i < how_many; i++) {
            new_items.push(new Model( {options: self[item_type + '_options']} ));
        }
        self[item_type](new_items);
    }

    self.initial_load = function() {
        self.get_data("recipes").done(self.recipes_loaded_success_callback);
        self.get_data("items/fermentables").done(function(a,b,c) { self.items_loaded(a,b,c, 'fermentables', Fermentable, 2) });
        self.get_data("items/hops").done(function(a,b,c) { self.items_loaded(a,b,c, 'hops', Hop, 2) });
        self.get_data("items/yeasts").done(function(a,b,c) { self.items_loaded(a,b,c, 'yeasts', Yeast, 1) });
    }

    self.initial_load();

    // somehow, these 2 functions have to be instantiated only after all 3 item callbacks are finished
    // maybe chain them together and do a final .done?

    // self.prices_hash = ko.computed(function(){
    //     var data = {};
    //     var strings = ['fermentables', 'hops', 'yeasts'];
    //     for (i = 0; i < strings.length; i++) {
    //         var string = strings[i];
    //         var attr = strings[i] + '_options';
    //         for (j = 0; j < self[attr].length; j++) {
    //             var groups = self[attr][j][string];
    //             for (k = 0; k < groups.length; k++) {
    //                 var catalog_id = groups[k].catalog_id.toString();
    //                 var current_price = groups[k].price;
    //                 data[catalog_id] = current_price;
    //             }
    //         }
    //     }
    //     return data;
    // });
    //
    // self.current_price = ko.computed(function(){
    //     var total_price = 0;
    //     var strings = ['fermentables', 'hops', 'yeasts'];
    //     for (i = 0; i < strings.length; i++) {
    //         /* for each item in valid_fermentables, valid_hops, and valid_yeasts,
    //          add the price (if there is one) to total_price */
    //         var attr = strings[i];
    //         var these_valid_items = self.valid_items( self[attr]() ); // self.yeasts, self.hops, etc
    //         for (j = 0; j < these_valid_items.length; j++){
    //             var item = these_valid_items[j];
    //             total_price = total_price + self.prices_hash()[item.catalog_id()];
    //         }
    //     }
    //
    //     return total_price.toFixed(2);
    // });

    self.reset_form = function(){
        self.id(null)
        self.name("");
        self.brew_method("Extract");
        self.batch_size("5");
        self.beer_style("Standard American Beer");
        self.boil_time("60");
        self.notes("");
        self.fermentables(
            [
                new Fermentable({options: self.fermentables_options}),
                new Fermentable({options: self.fermentables_options})
            ]
        );
        self.hops([new Hop({options: self.hops_options}), new Hop({options: self.hops_options})]);
        self.yeasts([new Yeast({options: self.yeasts_options})]);
    }

    self.populate_recipe = function(data, event){
        var context = ko.contextFor(event.target);
        var index = context.$index();
        var recipe = self.recipes()[index];
        var attrs = ['id', 'name', 'brew_method', 'boil_time', 'batch_size', 'notes']
        for (i = 0; i < attrs.length; i++) {
            attr = attrs[i];
            self[attr](recipe[attr]);
        }

        var populate_items = function(item_type, Model) {
            var data = recipe[item_type + '_selections'] // like fermentables_selections
            var new_data = [];
            for (k = 0; k < data.length; k++) {
                // makes a new Fermentable, Hop, or Yeast object
                var data_set = data[k];
                data_set['options'] = self[item_type + '_options'] // like fermentables_options
                var this_item = new Model(data_set);
                new_data.push(this_item);
            }
            self[item_type](new_data);
        }

        var populate_items_data = [
            {item_type: 'fermentables', model: Fermentable},
            {item_type: 'hops', model: Hop},
            {item_type: 'yeasts', model: Yeast},
        ]

        for (n = 0; n < populate_items_data.length; n++) {
            var this_data = populate_items_data[n];
            populate_items(this_data.item_type, this_data.model);
        }
    }

    self.valid_items = function(items){
        var undestroyed_items = _.filter(items, function(item){
            return !("_destroy" in item);
        });

        var final_items = _.filter(undestroyed_items, function(item){
            return item.is_valid();
        });
        return final_items;
    }

    self.addItem = function(item_name) {
        var item_data = {fermentables: Fermentable, hops: Hop, yeasts: Yeast};
        var Model = item_data[item_name];
        self[item_name].push(new Model({options: self[item_name + '_options']}));
    }

    // http://stackoverflow.com/questions/40501838/pass-string-parameters-into-click-binding-while-retaining-default-params-knockou
    self.removeItem = function(item, item_name){
        self[item_name].destroy(item);
    }

    self.purify_items = function(items, attrs){
        // TODO: check if can be replaced by pluck or something similar in _.js
        /* retrieves only the relevant attributes from a list of objects
         given a list like
           [ {catalog_id: 404, weight: 3, minutes: 45, use: 'Dry hop'}, ]
         and attrs ['catalog_id', 'weight'] it will return only
           [ {catalog_id: 404, weight: 3}, ]
         */
        var final_items = [];
        for (i = 0; i < items.length; i++){
            var item = items[i];
            var object = {};
            for (j = 0; j < attrs.length; j++) {
                var attr = attrs[j];
                object[attr] = item[attr];
            }
            final_items.push(object);
        }
        return final_items;
    }

    self.prepareJSON = function(){
        // pure as in only the fields the server cares about
        var pure_fermentables = self.purify_items(self.valid_items( self.fermentables() ), ['catalog_id', 'milling_preference']);
        var pure_hops = self.purify_items(self.valid_items( self.hops() ), ['catalog_id', 'weight', 'minutes', 'use']);
        var pure_yeasts = self.purify_items(self.valid_items( self.yeasts() ), ['catalog_id']);

        object = {
            recipe_id: self.id(),
            fermentables: pure_fermentables,
            hops: pure_hops,
            yeasts: pure_yeasts,
            name: self.name(),
            brew_method: self.brew_method(),
            batch_size: self.batch_size(),
            beer_style: self.beer_style(),
            boil_time: self.boil_time(),
            notes: self.notes(),
        }
        return object;
    }

    self.updateRecipes = function() {
        self.recipes_loaded(false);
        self.get_data("recipes").done(self.recipes_loaded_success_callback);
    }

    self.saveRecipeData = function(){
        var recipe_data = ko.toJSON(self.prepareJSON());
        // http://stackoverflow.com/questions/28921127/how-to-wait-for-a-javascript-promise-to-resolve-before-resuming-function
        // https://developers.google.com/web/fundamentals/getting-started/primers/promises
        var call_save = function(the_recipe_data) {
            if (self.id() == '' || self.id() == null) {
                return $.ajax({
                    // url: API_BASE +  "/api/recipes/",
                    url: "/api/recipes/",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "POST",
                    dataType: "json",
                    data: the_recipe_data,
                    complete: function(data) {
                        self.updateRecipes();
                    }
                });
            } else {
                var recipe_id = self.id();
                return $.ajax({
                    url: "/api/recipes/%".replace('%', recipe_id ),
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "PUT",
                    dataType: "json",
                    data: the_recipe_data,
                    complete: function(data) {
                        self.updateRecipes();
                    }
                    // error: function(e) {
                    //     console.log(e);
                    // }
                });
            }
        }

        call_save(recipe_data);
    }

    self.populate_fake_recipe_item_and_get_cookie = function(){
        var add_dummy_recipe_item_to_cart_url = "http://sandbox-homebrewing-com.3dcartstores.com/add_cart.asp?out=1&quick=1&item_id=6832";
        $.ajax({
            method: "POST",
            url: add_dummy_recipe_item_to_cart_url,
            success: function(output, status, xhr){
                console.log('Making ajax call to add fake recipe item to cart...');
                alert(xhr.getResponseHeader('Set-Cookie'));
            },
            complete: function(){
                alert('populate recipe item done');
            }
        });
    }

    self.populate_fake_recipe_item_and_get_cookie()

    self.add_to_cart_test = function(data, event){
        // http://stackoverflow.com/questions/3709597/wait-until-all-jquery-ajax-requests-are-done

        // http://stackoverflow.com/questions/5627284/pass-in-an-array-of-deferreds-to-when

        // Pass array of deferreds to $.when:
        //   http://jsfiddle.net/YNGcm/21/

        // data comes in like
        //   {'catalog_id': 234, 'quantity': 3}
        var add_to_cart_url = "http://sandbox-homebrewing-com.3dcartstores.com/add_cart.asp?out=1&quick=1&item_id=%the_id%";

        var ITEMS_LIST = ['fermentables', 'hops', 'yeasts'];

        var deferreds = [];

    //
        // for each item type: hops, yeasts, fermentables...
        for (i = 0; i < ITEMS_LIST.length; i++){
            var attr = ITEMS_LIST[i];
            var these_items = self[attr].getVisible();
            console.log("these_items: ", these_items);
            // for each item selected to be ordered...
            for (j = 0; j < these_items.length; j++){
                var this_item = these_items[j];
                var this_catalog_id = this_item.catalog_id();
                var this_quantity = this_item.quantity();
                // a for loop to make a call for each one to be ordered:
                // quantity = 4 makes 4 calls, etc...
                console.log('about to make ajax call for catalog_id ', this_catalog_id);
                if (!isNaN(this_catalog_id)){
                    // this keeps track or orders so we know the usage
                    var order_track_url = "http://127.0.0.1:8000/api/orders/%catalog_id%/%quantity%".replace('%catalog_id%', this_catalog_id).replace('%quantity%', this_quantity);
                    console.log(order_track_url);

                    $.ajax({
                        method: "POST",
                        url: order_track_url
                    });

                    for (n = 0; n < this_quantity; n++){
                        var this_deferred = $.ajax({
                            method: "GET",
                            url: add_to_cart_url.replace('%the_id%', this_catalog_id),
                            complete: function(){
                                console.log('Making ajax call to add to cart...');
                            }
                        });
                        deferreds.push(this_deferred);
                    }
                }
            }
        }

        $.when.apply($, deferreds).done(function(){
            console.log('Deferreds are done!');
            // refresh page
            // http://stackoverflow.com/questions/5294842/refresh-a-page-using-javascript-or-html
            // location = location;
        });

    }

    self.delete_recipe = function(data, event){
        console.log("running delete_recipe");
        var recipe_id = data.id;
        // var URL = API_BASE + "/api/recipes/&/".replace("&", recipe_id);
        var URL = "/api/recipes/&/".replace("&", recipe_id);
        $.ajax({
            url: URL,
            headers: {
                "Content-Type": "application/json"
            },
            // async: false,
            method: "DELETE",
            dataType: "json",
            complete: function(data) {
                self.updateRecipes();
            }
        });

        // self.get_data("recipes").done(self.recipes_loaded_success_callback);
    }
}
ko.applyBindings(new RecipeViewModel());
