from django.contrib.auth.models import User

from api.serializers import ItemRecordSerializer


def pluck(items, attr):
    # TODO: make it work with python objects to (getattr)
    attrs = [item[attr] for item in items]
    return attrs

def retrieve_all_objects_and_serialize(Model):
    serializer = ItemRecordSerializer(Model.objects.all(), many=True)
    return serializer.data

def unpack_json(request):
    body = request.body
    try:
        data = json.loads(body)
    except TypeError:
        str_response = response.readall().decode('utf-8')
        obj = json.loads(str_response)

def parse_keys(dictionary, keys):
    new_dict = {}
    for key in keys:
        new_dict[key] = dictionary[key]
    return new_dict

def get_example_user():
    users = User.objects.filter(username='username', password='password')
    if not users:
        user = User(username='username', password='password', email='fake@example.com')
        user.save()
    else:
        user = users[0]
    return user
