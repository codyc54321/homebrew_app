from api.models import (
    FermentableRecord, HopRecord, YeastRecord,
    FermentableSelection, HopSelection, YeastSelection
)

#dictoniary of three types, with dictionary inside
ITEMS_MODELS_MAP = {
    'fermentables': {'record_model': FermentableRecord, 'selection_model': FermentableSelection},
    'hops': {'record_model': HopRecord, 'selection_model': HopSelection},
    'yeasts': {'record_model': YeastRecord, 'selection_model': YeastSelection},
}
