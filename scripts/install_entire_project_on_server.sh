# from naked ubuntu server
cd

HOMEBREW_APP_ROOT="$HOME/homebrew_app"

# export PYTHONPATH="${PYTHONPATH}:$HOMEBREW_APP_ROOT"

sudo apt-get update
sudo apt-get install git nginx python-pip python3-pip libpq-dev python-dev -y
git clone https://bitbucket.org/codyc54321/homebrew_app # codyc54321, Apeshit9

sudo apt-get install libmysqlclient-dev mysql-client-core-5.6 mysql-client-5.6 mysql-common mysql-server-core-5.6 mysql-server-5.6 -y

# sudo mysql -u root -p -e 'create database homebrew;'
sudo mysql -u root -p -e  "create database homebrew; create user 'cchilders'@'localhost' identified by 'childers9'; grant all privileges on homebrew.* to 'cchilders'@'localhost' identified by 'childers9';"

sudo pip install virtualenv gunicorn

virtualenv -p `which python3` env

VENV_PIP_PATH="$HOME/env/bin/pip3"
VENV_PYTHON_PATH="$HOME/env/bin/python3"

$VENV_PIP_PATH install -r homebrew_app/requirements.txt

$VENV_PYTHON_PATH $HOMEBREW_APP_ROOT/manage.py makemigrations
$VENV_PYTHON_PATH $HOMEBREW_APP_ROOT/manage.py migrate

cd $HOMEBREW_APP_ROOT

gunicorn homebrew_app.wsgi:application --bind=0.0.0.0:80
