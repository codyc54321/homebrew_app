wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-139.0.1-linux-x86_64.tar.gz
sudo tar -xzf google-cloud-sdk-139.0.1-linux-x86_64.tar.gz --directory $HOME
sudo $HOME/google-cloud-sdk/install.sh
sudo $HOME/google-cloud-sdk/bin/gcloud init
rm google-cloud-sdk-139.0.1-linux-x86_64.tar.gz

sudo $HOME/google-cloud-sdk/bin/gcloud components install alpha beta docker-credential-gcr kubectl app-engine-python

gcloud auth application-default login
