PROJECT_ID="homebrew-test"
CLUSTER_NAME="homebrew"
BUCKET_NAME="homebrew-test"
# needs virtualenv
python manage.py collectstatic
# gcloud commands don't work with python3
gsutil mb gs://$BUCKET_NAME
gsutil defacl set public-read gs://$BUCKET_NAME
gsutil rsync -R static/ gs://$BUCKET_NAME/static
gcloud container clusters get-credentials $CLUSTER_NAME
docker build -t gcr.io/$PROJECT_ID/$CLUSTER_NAME . # homebrew-test = project id, homebrew = cluster name
gcloud docker push gcr.io/$PROJECT_ID/$CLUSTER_NAME
kubectl delete replicationcontrollers $CLUSTER_NAME
kubectl delete services $CLUSTER_NAME
kubectl create -f $HOME/projects/homebrew_app/k8s/dev/homebrew.yaml

sleep 15
kubectl get services $CLUSTER_NAME
