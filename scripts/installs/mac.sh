brew install mysql-server-5.7 mysql-server-core-5.7 mysql-client-5.7  mysql-client-core-5.7 mysql-common
# try to automate make db from command line on ma...this works on ubuntu
sudo mysql -u root -e 'create database homebrew;'
sudo mysql -u root -e  "create user 'pick_a_username'@'localhost' identified by 'pick_a_pw';"
sudo mysql -u root -e "grant all privileges on homebrew.* to 'the_username'@'localhost' identified by 'the_pw_you_made';"
mkdir projects # or start in your normal location
cd projects
git clone git@bitbucket.org:codyc54321/homebrew_app.git
cd homebrew_app
# get pip?
brew install python3
brew install python-pip
sudo pip install virtualenv # ? does this work
virtualenv -p `which python3` env
source env/bin/activate
pip3 install -r requirements.txt

# update the db
python3 manage.py makemigrations
python3 manage.py migrate

# initially populate the db. this is also the command we'll run on server every hour to update db for any changes from 3dcart
python3 manage.py update_item_records

# some tests will fail right now. at least 17 should pass
python manage.py test

# this dev server is good enough to see the page
python manage.py runserver
# now visit 127.0.0.1:8000/main
