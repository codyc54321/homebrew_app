sudo apt-get install mysql-server-5.7 mysql-server-core-5.7 mysql-client-5.7  mysql-client-core-5.7 mysql-common
sudo mysql -u root -e 'create database homebrew;'
sudo mysql -u root -e  "create user 'pick_a_username'@'localhost' identified by 'pick_a_pw';"
sudo mysql -u root -e "grant all privileges on homebrew.* to 'the_username'@'localhost' identified by 'the_pw_you_made';"
mkdir projects # or start in your normal location
cd projects
git clone git@bitbucket.org:codyc54321/homebrew_app.git
cd homebrew_app
sudo apt-get install python-pip
sudo pip install virtualenv
# this won't work on zsh...idk how to expand and run bash command besides `ls` and $(ls) styles...
virtualenv -p `which python3` env
source env/bin/activate
pip3 install -r requirements.txt

# update the db
python3 manage.py makemigrations
python3 manage.py migrate

# some tests will fail right now. at least 17 should pass
python manage.py test

# this dev server is good enough to see the page
python manage.py runserver
# now visit 127.0.0.1:8000/main
