ACCESS_TOKEN="$(gcloud auth application-default print-access-token)"

PROJECT_ID="homebrew-app"
INSTANCE_NAME="homebrew-db-server"
DATABASE_NAME="homebrew-db"

TIER="db-fi-micro"

# https://cloud.google.com/sql/docs/mysql/create-instance

# make a database server:
gcloud beta sql instances create $INSTANCE_NAME --tier=$TIER --activation-policy=ALWAYS

# set pw:
gcloud sql instances set-root-password $INSTANCE_NAME --password childers9

gcloud beta sql instances describe $INSTANCE_NAME

# https://cloud.google.com/sql/docs/mysql/create-manage-databases

# create the database on your Cloud SQL server:
# curl --header "Authorization: Bearer ${ACCESS_TOKEN}" \
#      --header 'Content-Type: application/json' \
#      --data '{"project": "${PROJECT_ID}", "instance": "{INSTANCE_NAME}", "name": "{DATABASE_NAME}"}' \
#      https://www.googleapis.com/sql/v1beta4/projects/$PROJECT_ID/instances/$INSTANCE_NAME/databases -X POST
