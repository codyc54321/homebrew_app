# from unittest.mock import MagicMock

from copy import deepcopy
from decimal import Decimal

from django.test import TestCase
from faker import Faker
FAKER = Faker()

from utils.maps import ITEMS_MODELS_MAP
from api.three_d_cart_client import ThreeDCartAPIClient
from api.models import (
    FermentableRecord, HopRecord, YeastRecord
    # FermentableSelection, HopSelection, YeastSelection
)
from tests.test_api.api_tests_utils import create_fake_item_record, populate_entire_fixture
from tests.test_api.FIXTURES_AND_EXPECTATIONS import (
    TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE,
)

api_client = ThreeDCartAPIClient()


class Test3DCartApiClient(TestCase):

    def setUp(self):
        self.api_client = api_client
        self.URL_BASE = "https://apirest.3dcart.com/3dCartWebAPI/v1"
        self.required_processed_category_keys = set(['category_id', 'parent', 'title', 'name'])
        self.old_keys = set(['CategoryName', 'CategoryID', 'CategoryParent', 'Title'])
        self.main_category_ids = set([1, 16, 487])

    def test_update_all_item_records(self):
        self.api_client.update_all_item_records()
        self.assertGreater(FermentableRecord.objects.all().count(), 180)
        self.assertGreater(HopRecord.objects.all().count(), 100)
        self.assertGreater(YeastRecord.objects.all().count(), 180)

    def test_update_retrieved_item_records_to_db_adds_new_items_to_db(self):
        NAME = 'this test grain'
        PRICE = 1.23
        populate_entire_fixture(TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE)
        self.assertEqual(len(FermentableRecord.objects.all()), 3)
        this_fixture = deepcopy(TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE)
        this_fixture[0]['fermentables'].append( {'catalog_id': 999, 'name': NAME, 'price': PRICE} )
        self.api_client.update_retrieved_item_records_to_db(this_fixture, FermentableRecord, 'fermentables')
        self.assertEqual(len(FermentableRecord.objects.all()), 4)
        record = FermentableRecord.objects.get(catalog_id=999)
        self.assertEqual(record.name, NAME)
        self.assertEqual(record.price, Decimal('1.23'))

    def test_update_retrieved_item_records_to_db_overwrites_changed_items(self):
        # overwrites items that existed in our db but changed in 3D Cart
        this_fixture = deepcopy(TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE)
        NEW_NAME = 'this new name'
        NEW_PRICE = 2.44
        populate_entire_fixture(this_fixture)
        self.assertEqual(len(FermentableRecord.objects.all()), 3)
        this_fixture[0]['fermentables'][0]['name'] = NEW_NAME
        this_fixture[0]['fermentables'][0]['price'] = NEW_PRICE
        self.api_client.update_retrieved_item_records_to_db(this_fixture, FermentableRecord, 'fermentables')
        self.assertEqual(len(FermentableRecord.objects.all()), 3)
        record = FermentableRecord.objects.get(catalog_id=496)
        self.assertEqual(record.name, NEW_NAME)
        self.assertEqual(record.price, Decimal('2.44'))
        names = set(FermentableRecord.objects.values_list('name', flat=True))
        expected_names = {'this new name', 'Briess Bavarian Malt DME 1 Lb', 'Wildflower Honey 1 Lb'}
        self.assertEqual(names, expected_names)

    def test_update_retrieved_item_records_to_db_sets_deleted_items_to_inactive(self):
        # assert that it checks for items in our db, but no longer in 3d cart,
        # and sets them to 'inactive=True'
        this_fixture = deepcopy(TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE)
        populate_entire_fixture(this_fixture)
        self.assertEqual(len(FermentableRecord.objects.all()), 3)
        self.assert_expected_active_value(333)
        self.assert_expected_active_value(496)
        self.assert_expected_active_value(555)
        del this_fixture[0]['fermentables'][1]
        self.api_client.update_retrieved_item_records_to_db(this_fixture, FermentableRecord, 'fermentables')
        self.assertEqual(len(FermentableRecord.objects.all()), 3)
        self.assert_expected_active_value(333, active=False)
        self.assert_expected_active_value(496)
        self.assert_expected_active_value(555)

    def assert_expected_active_value(self, catalog_id, active=True):
        record = FermentableRecord.objects.get(catalog_id=catalog_id)
        self.assertEqual(record.active, active)

    def test_update_retrieved_item_records_to_db_sets_soldout_items_to_soldout(self):
        assert 1 == 1
        # may want to talk to J or Ben about this
        # this feature wasn't requested but seems good

    def test_pluck_items_and_rename_them_fermentables(self):
        assert 1 == 1

    def test_pluck_items_and_rename_them_hops(self):
        assert 1 == 1

    def test_pluck_items_and_rename_them_yeasts(self):
        assert 1 == 1

    def test_add_products_to_final_beer_categories(self):
        final_data = self.api_client.final_beer_categories_including_products
        grains = self.api_client.retrieve_from_data(487, final_data, key="category_id")
        grain_subcategories = grains['subcategories']
        dry_malt_products = self.api_client.retrieve_from_data("Dry Malt Extract", grain_subcategories, key="name")['products']
        self.assertIn({'catalog_id': 496, 'name': 'Briess Bavarian Wheat DME 1 Lb', 'price': 4.99}, dry_malt_products)

    def test_filter_gathered_products_to_only_unique_products(self):
        assert 1 == 1

    def test_gather_products_for_a_subcategory(self):
        assert 1 == 1

    def test_format_final_beer_categories(self):
        final_categories = self.api_client.load_from_yaml(self.api_client.FINAL_BEER_CATEGORIES_PATH)
        self.assertEqual(len(final_categories), 3)

        final_ids = [item['category_id'] for item in final_categories]
        self.assertEqual(set(final_ids), self.main_category_ids)

        grains = self.api_client.retrieve_from_data(487, final_categories, key="category_id")
        expected_grains = [
            {'title': 'Dry Malt Extract - DME - Dried Barley and Wheat Extract', 'parent': 487, 'name': 'Dry Malt Extract', 'category_id': 396},
            {'title': 'Honey for Brewing Beer and Mead at Home', 'parent': 487, 'name': 'Honey for Brewing', 'category_id': 65},
            {'title': 'Liquid Malt Extract - Barley LME and Wheat Extract Syrup', 'parent': 487, 'name': 'Liquid Malt Extract', 'category_id': 193},
            {'title': 'Sugars for Brewing Beer - Homebrew Sugar', 'parent': 487, 'name': 'Sugars for Brewing', 'category_id': 153}
        ]
        grain_categories = grains['subcategories']
        self.check_for_items(expected_grains, grain_categories)

    def test_process_product(self):
        product = self.api_client.load_from_yaml(self.api_client.generate_yaml_path('info/one_product.yaml'))
        processed_product = self.api_client.process_product(product)
        expectation = {'name': "Mangrove Jack's US West Coast M44 Dried Yeast", 'catalog_id': 4235, 'price': 3.99}
        self.assertEqual(processed_product, expectation)

    def test_process_beer_ingredients_subcategories(self):
        processed_beer_categories = self.api_client.process_beer_ingredients_subcategories(
            self.api_client.unprocessed_beer_categories,
            self.api_client.all_categories
        )
        white_labs = [cat for cat in processed_beer_categories if cat['category_id'] == 2][0]
        wyeast = [cat for cat in processed_beer_categories if cat['category_id'] == 12][0]
        self.assertGreaterEqual(len(white_labs['subcategory_ids']), 9)
        self.assertGreaterEqual(len(wyeast['subcategory_ids']), 9)

    def test_filter_to_beer_ingredient_categories(self):
        ids = [item['category_id'] for item in self.api_client.beer_categories]
        ids = set(ids)
        from_yaml = self.api_client.load_from_yaml(self.api_client.BEER_CATEGORIES_PATH)
        ids_from_yaml = [item['category_id'] for item in from_yaml]
        ids_from_yaml = set(ids_from_yaml)
        self.assertEqual(ids, ids_from_yaml)

    def test_process_category_keys(self):
        processed_categories = self.api_client.all_categories
        some_categories = processed_categories[:20]
        self.check_processed_categories_keys(processed_categories)

    def test_processed_categories_keys_yaml(self):
        processed_categories = self.api_client.load_from_yaml(self.api_client.ALL_CATEGORIES_PATH)
        self.check_processed_categories_keys(processed_categories)

    def check_processed_categories_keys(self, processed_categories):
        length = len(processed_categories)
        self.assertGreater(length, 300)
        some_categories = processed_categories[:20]
        required_keys = set(self.required_processed_category_keys)

        for c in some_categories:
            self.check_for_items(self.required_processed_category_keys, c.keys())

            for key in self.old_keys:
                self.assertNotIn(key, c.keys())

    def test_process_object_data(self):
        category_data = self.api_client.process_object_data(
            self.api_client.load_from_yaml(self.api_client.RAW_CATEGORIES_PATH),
            [
                ('name', 'CategoryName'), ('title', 'Title'),
                ('category_id', 'CategoryID'), ('parent', 'CategoryParent')
            ]
        )

        old_keys = ['Title', 'CatagoryID', 'CategoryName', 'CategoryParent']

        new_keys = set(['name', 'title', 'parent', 'category_id'])

        for item in category_data:
            for key in old_keys:
                self.assertNotIn(key, item.keys())
            self.assertEqual(new_keys, set( item.keys() ) )

    """ takes 50 seconds """
    # def test_get_categories_live_api(self):
    #     categories = self.api_client.get_categories_live_api()
    #
    #     length = len(categories)
    #     self.assertGreater(length, 300)
    #
    #     some_categories = categories[:20]
    #
    #     required_keys = set(['CategoryDescription', 'CategoryID', 'Title'])
    #     for c in some_categories:
    #         keys_set = set(c.keys())
    #         self.assertTrue(required_keys.issubset(keys_set))

    def test_get_products(self):
        products = self.api_client.get_products(153) # sugars
        self.assertGreaterEqual(len(products), 25)
        self.assertLessEqual(len(products), 30)

        a_product = products[8]
        self.assertIn('PriceLevel9', a_product.keys())
        self.assertIn('LoginRequiredOptionRedirectTo', a_product.keys())
        self.assertIn('RMAMaxPeriod', a_product.keys())

    def test_get_response(self):
        url_args = ('Categories', '270', 'Products')
        white_labs_yeasts = self.api_client.get_response(url_args)
        response_titles = [item['Title'] for item in white_labs_yeasts]
        expected_titles = [
            'White Labs 001 California Ale Yeast - WLP001',
            'White Labs 008 East Coast Ale Yeast - WLP008',
            'White Labs 051 California Ale V Yeast - WLP051',
            'White Labs 320 American Hefeweizen Yeast - WLP320',
            'White Labs 810 San Francisco Lager Yeast - WLP810',
            'White Labs 840 American Lager Yeast - WLP840',
            'White Labs 041 Pacific Ale Yeast - WLP041',
            'White Labs 940 Mexican Lager Yeast - WLP940',
            'White Labs 060 American Ale Yeast Blend - WLP060',
            'White Labs 080 Cream Ale Yeast',
            'White Labs 670 American Farmhouse Yeast'
        ]
        for title in expected_titles:
            self.assertIn(title, response_titles)

    def test_get_target_url(self):
        url = self.api_client.get_target_url( ('Categories', 'Stuff', '298'), limit=555)
        expectation = self.URL_BASE + "/Categories/Stuff/298?limit=555"
        self.assertEqual(url, expectation)

    def test_return_data(self):
        url = self.URL_BASE + "/Categories/2/Products"
        data = self.api_client.return_data(url)
        # check for item 6770 in the data
        worked = False
        for item in data:
            if item['SKUInfo']['CatalogID'] == 6770:
                worked = True
        self.assertTrue(worked)

    def check_for_items(self, items, target_list):
        for item in items:
            self.assertIn(item, target_list)
