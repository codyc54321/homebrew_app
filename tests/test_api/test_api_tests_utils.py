from decimal import Decimal

from django.test import TestCase

from api.models import (
    FermentableRecord, HopRecord, YeastRecord
    # FermentableSelection, HopSelection, YeastSelection
)
from tests.test_api.FIXTURES_AND_EXPECTATIONS import (
    TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE,
)
from .api_tests_utils import create_fake_item_record, populate_entire_fixture


class TestApiTestsUtils(TestCase):

    def test_create_fake_item_record(self):
        item = create_fake_item_record(FermentableRecord, 37)
        ferm_records = FermentableRecord.objects.all()
        assert len(ferm_records) == 1
        record = ferm_records[0]
        assert record.catalog_id == 37

    def test_populate_entire_fixture(self):
        fermentables_records = FermentableRecord.objects.all()
        self.assertEqual(len(fermentables_records), 0)
        populate_entire_fixture(TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE)
        fermentables_records = FermentableRecord.objects.all()
        self.assertEqual(len(fermentables_records), 3)
        record = FermentableRecord.objects.get(catalog_id=555)
        self.assertEqual(record.name, 'Wildflower Honey 1 Lb')
        record = FermentableRecord.objects.get(catalog_id=496)
        self.assertEqual(record.price, Decimal('4.99')) # DecimalField holds values like this
