
TWO_CATEGORY_THREE_ITEM_FERMENTABLES_FIXTURE = [
    {
        'category': 'Dry Malt Extract',
        'fermentables': [
            {'catalog_id': 496, 'name': 'Briess Bavarian Wheat DME 1 Lb', 'price': 4.99},
            {'catalog_id': 333, 'name': 'Briess Bavarian Malt DME 1 Lb', 'price': 4.99},
        ]
    },
    {
        'category': 'Honeys',
        'fermentables': [
            {'catalog_id': 555, 'name': 'Wildflower Honey 1 Lb', 'price': 6.99},
        ]
    },
]
