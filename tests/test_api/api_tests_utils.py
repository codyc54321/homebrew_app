from copy import deepcopy

from faker import Faker
FAKER = Faker()

from utils.maps import ITEMS_MODELS_MAP


def create_fake_item_record(Model, catalog_id, name=None, price=None,
        category=None, active=True, in_stock=True):
    if not name:
        name = FAKER.word()
    if not price:
        price = FAKER.numerify('##.##')
    if not category:
        category = FAKER.word()
    item = Model.objects.create(catalog_id=catalog_id, name=name, price=price, category=category)
    item.save()
    return item

def populate_entire_fixture(fixture):
    dataset = fixture[0]
    keys = list(dataset)
    keys.remove('category')
    item_type = keys[0]
    assert item_type == 'fermentables'
    for dataset in fixture:
        category = dataset['category']
        items_datasets = dataset[item_type]
        for items_dataset in items_datasets:
            dataset_copy = deepcopy(items_dataset)
            catalog_id = dataset_copy.pop('catalog_id')
            dataset_copy.update({'category': category})
            Model = ITEMS_MODELS_MAP[item_type]['record_model']
            create_fake_item_record(Model, catalog_id, **dataset_copy)
