#!/usr/bin/env python3

# http://brewgr.com/calculations/srm-beer-color


MAIN_DATA = {
    'gallons': 5,
    'grains': [
        {'lovibonds': 40, 'ounces': 32}, # weight in lbs
        {'lovibonds': 500, 'weight': 0.43},
    ]
}


def calculate_SRM_for_one_grain(data):
    """
    MCU = (Grain Color * Grain Weight lbs.)/Volume in Gallons

    This works great for beers that are light in color but due to the
    fact that light absorbance is logarithmic and not linear we need to use the Morey equation:

    SRM Color = 1.49 * (MCU * 0.69)
    """
    try:
        weight = data['weight']
    except KeyError:
        weight = data['ounces'] / 16
    MCU = ( data['lovibonds'] * weight ) / data['gallons']
    return MCU
    

def calculate_SRMs_for_recipe(data):
    SRMs = 0
    gallons, grains = data['gallons'], data['grains']
    
    def get_grain_data(data):
        grain_data = {'gallons': gallons}
        grain_data.update(data)
        return grain_data
        
    for grain in grains:
        SRMs += calculate_SRM_for_one_grain(get_grain_data(grain))
    
    return SRMs


SRMs = calculate_SRMs_for_recipe(MAIN_DATA)
print(SRMs)


    
# keep lines